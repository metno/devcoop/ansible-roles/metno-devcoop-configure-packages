# Description

Ansible role to configure basic packages for a Diana meteorological workstation.
This role is not very useful by itself, it should be part of a sequence of roles.
